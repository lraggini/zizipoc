import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class AddressService {

  constructor(
    private http: HttpClient
  ) { 

  }

  getAdresses(query: string) {
    const urlEncodedQuery = encodeURI(query);
    return this.http.get("https://api-adresse.data.gouv.fr/search/?q=" + urlEncodedQuery);
  }

}
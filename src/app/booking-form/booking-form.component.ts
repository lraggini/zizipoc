import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AddressService } from '../services/address.service';

@Component({
  selector: 'app-booking-form',
  templateUrl: './booking-form.component.html',
  styleUrls: ['./booking-form.component.css']
})
export class BookingFormComponent implements OnInit {
  bookingForm: FormGroup;
  startOptions: string[] = [];
  finishOptions: string[] = [];
  constructor(
    private fb: FormBuilder,
    private addressService: AddressService
  ) { 
    this.bookingForm = fb.group({
      'start' : ['', [Validators.required]],
      'finish'  : ['', [Validators.required]]
    });
  }

  ngOnInit() {
  }

  onStartKey(value: string) {    
    if (value.length > 3) {
      this.startOptions = [];
      this.addressService.getAdresses(value).subscribe(
        (data: any) => {          
          data.features.map(
            feature => this.startOptions.push(feature.properties.label)
          );
        }
      );
    }
  }

  onFinishKey(value: string) {
    if (value.length > 3) {
      this.finishOptions = [];
      this.addressService.getAdresses(value).subscribe(
        (data: any) => {
          data.features.map(
            feature => this.finishOptions.push(feature.properties.label)
          );
        }
      );
    }
  }
}
